const xlsxtojson = require('xlsx-to-json')

const upload = (req, res, next)  => {
    const file = req.file
    const sheet = req.body.sheet

    if (!file) {
        const error = new Error('Please upload a file')
        error.httpStatusCode = 400
        return next(error)
    }  

    xlsxtojson({
        input: file.path,  
        output: "output.json", // save output to the file
        sheet: sheet,
    }, function(err, result) {
        if(err) {
            res.json(err);
        } else {
            // remove empty key value from obj
            let data = result.map(item => {
                delete item[""]
                return item
            })

            res.send(data)
        }
    });
}
module.exports = { 
    upload
  };