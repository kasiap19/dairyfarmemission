const calculate = (req, res)  => {
    const { body } = req

    const emissions = {
        electricity: 0.23314,  // per kWh 
        diesel: 2.68787, // per liter
        food: 1.3289, // per kg
    } 
    
    let scope1, scope2, scope3
    
    let diesel = body["Diesel"]
    let electicity = body["Electricity"]["amount"] 
    let gras = body["Gras for cows"]
    let soy = body["Soy for cows"]

    // convert from gallon to l => 1 gallon = 3.8L
    if(diesel["unit"] === "gallon") {
        diesel["amount"] = diesel["amount"] * 3.8
    }

    // convert tons to kgs
    if(gras["unit"] === "ton" || soy["units"] === "tons") {
        gras["amount"] = gras["amount"] * 1000
        soy["amount"] = soy["amount"] * 1000 
    } 

    // calculate scopes
    scope1 = diesel["amount"] * emissions.diesel
    scope2 = electicity * emissions.electricity
    scope3 = (gras["amount"] + soy["amount"]) * emissions.food 
    
    let total = scope1 + scope2 + scope3
    let emissionPerMilk = (scope1 + scope2 + scope3) / body["milkProduced"]
    let obj = { 
        'Fossil Fuels': Math.round(scope1 * 10) / 10,
        'Electricity': Math.round(scope2 * 10) / 10, 
        'Food Purchases': Math.round(scope3 * 10) / 10,
        'Total Emission': Math.round(total * 10) / 10,
        'Emission Per Liter Of Milk': Math.round(emissionPerMilk * 10) / 10
    }

    res.send(obj)
} 

module.exports = {  
    calculate 
};