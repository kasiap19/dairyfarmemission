const uploadCsv = require('./controllers/upload'),
calculateResults = require('./controllers/calculator'),
express = require('express'),
cors = require('cors'),
multer  = require('multer')
var bodyParser = require('body-parser');

const app = express();
app.use(cors())
// app.use(express.urlencoded({ extended: true })); 
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// multer => handles upload file in node
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
      cb(null, 'uploads/')
  },
  filename: function(req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
  } 
});

// upload csv file
const upload = multer({storage});  
app.post('/upload', upload.single('file'), uploadCsv.upload)

// calculate
app.post('/calculate', calculateResults.calculate)

// listen on the port 
app.listen(8000);   