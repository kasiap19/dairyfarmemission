import axios from '../axios'

class UploadFileService {
    upload(file, sheet) {
        let formData = new FormData();
        formData.append("file", file);
        formData.append("sheet", sheet);

        return axios.post("/upload", formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            }, 
        }); 
    } 
}

export default new UploadFileService(); 