import axios from '../axios'

class CalculateService {
    calculate(data) { 
        return axios.post("/calculate", data, {
            headers: {
                "Content-Type": "application/json"
            }, 
        });   
    } 
 
}

export default new CalculateService(); 