Instructions to run the project:

**1st terminal:**
- navigate to client folder:
`cd client`

- install packages:
`npm install`

- run the vue project:
`npm run serve`

**2nd terminal:**
- navigate to server folder:
`cd server`

- install packages:
`npm install`

- run node script
`npm run start`


Project should be running at http://localhost:8080/

